import os
import datetime

from email import email
from logger.logger import load_logger

SERVERS_FOLDER = os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'servers')
LOG_FILE_NAME = os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'check_mule.log')

logger = None
messages = []

def quit_script():
    """Print time and exit current script."""
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    exit() 

def start():
    """Receive the logger object and print the initial time to log file."""
    global logger
    logger = load_logger(log_file_name=LOG_FILE_NAME)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def check_servers_list():
    """Check folder "servers" exists in current directory and has at least one 
    file.
    """
    if os.path.exists(SERVERS_FOLDER):
        if os.listdir(SERVERS_FOLDER) == "":
            logger.error('There are no servers listed on "servers" folder. ' + 
                         'Nothing to check.')
            quit_script()
    else:
        logger.error('Folder "servers" is missing. Nothing to check.')
        quit_script()

def get_servers_status():
    """
    Loop through every file (every server) in servers folder.
    First it reads the server file: if the value is 5, it means the server has 
    been running "normaly", and the CPU usage can be checked. If value is 10 
    or 15, it means the server has been restarted and there is no need to 
    check CPU usage. In this cases, value needs to be updated from 10 to 15, 
    or from 15 to 5.
    """
    servers_status = {}
    for server in os.listdir(SERVERS_FOLDER):
        try:
            server_file = os.path.join(SERVERS_FOLDER, server)
            with open(server_file, 'r') as sf:
                servers_status[server] = sf.readline().rstrip()
        except IOError:
            logger.error(server_file + ' could not be opened.')
    return servers_status

def action_server_status():
    servers_status = get_servers_status()
    switcher = {'5': action_5, '10': action_10, '15': action_15}
    for server,status in servers_status.iteritems():
        func = switcher.get(status, unknow_arg)
        logger.info("Checking server {0}".format(server))
        func(server)
    send_email()

def action_5(server):
    """Check CPU usage in mule server."""
    command = ("ssh {0} \"sar -u 1 3 | " + 
               "awk '{{usage=(\$5)}} END {{print usage }}'\"").format(server)
    CPU_pctg = 0
    try:
        CPU_pctg = 100 - float(os.popen(command).readline())
        if CPU_pctg >= 90:
            logger.warning(("Server {0} CPU usage is at {1}%. " + 
                "Server will be restarted...").format(server, CPU_pctg))
            try:
                # restart server
                command = ("ssh {0} \"cd /home/weblogic/mule_carrier/bin; " + 
                        "./mule restart > /dev/null 2>&1 &\"").format(server)
                os.popen(command)
                logger.warning(("Server {0} was restarted. " + 
                    "Please check log file: [MULE_HOME]/logs/mule_ee.log").\
                    format(server))
                # update file
                change_status(server, 10)
                # save message for sending email
                message = ("Mule server {0} was restarted due to high " + 
                           "CPU usage. Server was at {1}% at the time it " + 
                           "was checked.").format(server, CPU_pctg)
                messages.append(message)
            except BaseException, e:
                message = ("Mule server {0} is at {1}% CPU usage. " +
                    "Please note server COULD NOT be restarted by automated " +
                    "mechanism. This exception happened when trying to " + 
                    "restart server: {2}").format(server, CPU_pctg, e)
                logger.error(message)
                # save message for sending email
                messages.append(message)
        else:
            logger.info("Server {0} CPU usage is at {1}%".\
                        format(server, CPU_pctg))
    except ValueError:
        message = ("We couldn't connect to listed server {0}. " + 
                   "Is this a real server name?").format(server)
        logger.error(message)
        # save message for sending email
        messages.append(message)
    except Exception as e:
        message = ("While checking server {0}, this exception happened: " + 
                   "{1}").format(server, e)
        logger.error(message)
        # save message for sending email
        messages.append(message)

def action_10(server):
    change_status(server, 15)

def action_15(server):
    change_status(server, 5)

def unknow_arg(server):
    message = "Server {0} has an invalid status".format(server)
    logger.error(message)
    messages.append(message)

def change_status(server, status):
    try:
        server_file = os.path.join(SERVERS_FOLDER, server)
        with open(server_file, 'w') as sf:
            sf.write(str(status))
        logger.info("Status for server {0} was changed to {1}".\
                    format(server, status))
    except IOError:
        logger.error('{0} could not be updated with status {1}'.\
                    format(server_file, status))

def send_email():
    if messages:
        body = ""
        subject = "CheckMule Servers Alert"
        for message in messages:
            body += (message + "\n\n")
        email.send_email(subject, body, 
                    email_to='ypaz@tracfone.com')
