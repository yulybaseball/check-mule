import datetime
import os

from logger.logger import load_logger

EMAIL_FROM = "ProductionWebApplicationSupport@tracfone.com"
EMAIL_DATE = 666
EMAIL_TO = "ypaz@tracfone.com"

logger = None

def send_email(subject, body, email_to=EMAIL_TO):
    global logger
    logger = load_logger(log_file_name='check_changes.log')
    logger.info('Trying to send email...')

    # create file to be used as 'the email' itself
    date_time = str(datetime.datetime.now())
    replacements = {'.': '_', ' ': '_', ':': '_'}
    # unique email name by using date and time
    email_name = 'email_{0}{1}'.\
            format(''.join([replacements.get(c, c) for c in date_time]), '.em')

    full_file_path = create_file_email(email_name, subject, body, email_to)
    if full_file_path:
        logger.info('Using file: {0}'.format(full_file_path))
        cmd = "mailx -t < {0}".format(full_file_path)
        try:
            os.system(cmd)
            logger.info('Email was sent to {0}'.format(email_to))
        except BaseException, e:
            logger.error(e)
    else:
        logger.error('Message could not be sent: conf file doesn\'t exist.')

def create_file_email(email_name, subject, body, email_to):
    try:
        filepath = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'logs', email_name)
        with open(filepath, 'a+') as email_file:
            email_file.write(("From: {0}\nDate: {1}\nTo: {2}\n" + 
                              "Subject: {3}\n\n{4}\n").
                    format(EMAIL_FROM, EMAIL_DATE, email_to, subject, body))
    except BaseException, e:
        filepath = None
        logger.error(e)
    return filepath