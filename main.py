#!/bin/env python

import check_mule as cm

def main():
    cm.start()
    # check we have Mule servers listed
    cm.check_servers_list()

    # process
    cm.action_server_status()

    # finish... just in case it was not called before
    cm.quit_script()

if __name__ == "__main__":
    main()